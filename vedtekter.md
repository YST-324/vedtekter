# Clio Studentforenings vedtekter

Oppdatert 2. oktober 2021.
  
## §1 Om foreningen 

1.1 Clio Studentforening er en frivillig studentorganisasjon for historiestudentene på Universitetet i Agder. Foreningen ble stiftet 11.10.2017. 

1.2 Clio Studentforening er en frittstående interesseorganisasjon, og er et politisk og religiøst uavhengig organ. 

## §2 Om formålet 

2.1 Denne foreningen blir stiftet for å fremme engasjement, et godt læringsmiljø og fellesskap mellom historiestudentene på Universitetet i Agder. 

## §3 Om organisasjonen 

3.1 Clio Studentforening består av et styre på minimum 3 personer: 

<ol>
  <li>Leder</li>
    <ol>
       <li>Juridisk og økonomisk ansvarlig. Innkaller og leder styremøter. Har ansvar for kommunikasjonen med eksterne parter, samarbeidspartnere, fakultetsstyret og leverandører. Har signaturrett. Ansvar for at ulike frister overholdes.</li>
    </ol>
  <li>Nestleder</li>
    <ol>
      <li>Leders stedfortreder. Referent ved styremøter. Ansvar for innkalling og gjennomføring av årsmøte.</li>
    </ol>
  <li>Økonomiansvarlig</li>
    <ol>
      <li> Ansvarlig for sunn økonomisk drift av foreningen. Legge frem budsjetter og regnskap ved hvert årsmøte. Ansvar for bilagsføring og foreningens konto. I samarbeid med styreleder sørge for at søknad om økonomiske tilskudd blir gjort innenfor fristene.</li>
    </ol>
  <li>Ansvarlig for tverrfaglig samarbeid</li>
    <ol>
      <li>Ansvarlig for samarbeid med de andre linjeforeningene på Fakultet for humaniora og pedagogikk. Herunder planlegging av felles arrangement, møtevirksomhet og opprettholdelse av god kontakt mellom linjeforeningene. Styremedlemmet rapporterer til resten av styret på styremøtene.</li>
    </ol>
  <li>Medieansvarlig</li>
    <ol>
      <li>Skal ha ansvaret for alt av markedsføring i media. Skal opprette arrangement ca én uke før arrangementer etter kommunikasjon med arrangementansvarlig, påminnelse tre dager før. Ta eller motta bilder fra arrangement og legge ut på Facebook etter avholdt arrangement.</li>
    </ol>
  <li>Arrangementansvarlig</li>
    <ol>
      <li>Ansvarlig for gjennomføring av arrangement planlagt i fellesskap av styret på styremøter. Ansvaret består av delegering av oppgaver til arrangement (innkjøp, diverse forberedelser) og samarbeide med medieansvarlig ang opprettelse av arrangement på sosiale medier. Ansvar for å føre arrangementsrapport. Skal holde kontroll på at alt av forberedelser til de forskjellige arrangementene blir gjennomført.</li>
    </ol>
  <li>Styremedlem</li>
    <ol>
      <li>Et styremedlem. Skal bistå styret med diverse oppgaver de får ansvar for, for eksempel ved arrangementer. Skal møte på styremøter og har stemmerett.</li>
    </ol>
</ol> 

3.2 Alle verv i styret velges på årsmøtet eller ved behov. 

3.3 Styret i Clio Studentforening er beslutningsdyktige når 2/3 av styremedlemmene er tilstede. Saker i styret vedtas ved simpelt flertall. Ved stemmelikhet har leder dobbeltstemme 

3.4 Styrets leder, eller nestleder i dens fravær, eller den styrets leder bemyndiger, har signaturrett på vegne av foreningen ved avtaler med eksterne parter som binder foreningen på noen som helst måte. 

3.5 Alle styreverv i Clio Studentforening er ulønnet og forplikter seg til å sitte minimum en periode. 

3.6 Det er leders overordnede ansvar å sørge for at alle personer i styre jobber etter foreningens formål og deres konkrete stillingsbeskrivelser, samt ivaretar foreningens gode rykte.  

3.7 Medlemmer av styret må være studenter ved Universitetet i Agder, og må ha betalt semesteravgiften. Alle styrets medlemmer må gå på enten årsstudium-, bachelor-, eller master i historie. 

  

## §4 Økonomi 

4.1 Foreningens midler skal kun brukes i henhold til foreningens formål. 

4.2 Der skal alltid foreligge budsjett og regnskap for foreningens midler, med tilhørende billag. Gjennomgåing og godkjenning av budsjett og regnskap skal gjøres sammen med styret før hvert årsmøte. 

4.3 Utbetalinger skal alltid godkjennes av leder – eller leders stedfortreder – i tillegg til økonomiansvarlig. 

4.4 Ved stemming over økonomiske investeringer trengs det et 2/3 flertall i styret. 

4.5 Økonomiansvarlig har ansvar for oppfølging av alle økonomiske saker, samt plikter å informere styret fortløpende om økonomibruken. 

## §5 Om generalforsamling 

5.1 Årsmøtet er foreningens øverste organ. 

5.2 Årsmøtet skal holdes minimum en gang i skoleåret. 

5.3 Årsmøte fatter vedtak ved simpelt flertall. Ingen medlemmer har mer enn en stemme og stemmegivning kan ikke skje ved fullmakt. 

5.4 Ekstraordinært årsmøte kan innkalles hvis minimum 1/3 av medlemmene krever det eller styret finner det nødvendig. 

5.5 Innkalling til årsmøtet skal være medlemmene i hende minimum to uker før årsmøtet. Sakspapirer skal være tilgjengelig på nett eller ved forespørsel minimum en uke før. Vedtektsendringer og øvrige saker som skal stemmes over på årsmøtet må være styret i hende minimum en uke før årsmøtet og vedtektsendringer kan bare skje når 2/3 av medlemmene stemmer for. 

5.6 Saksliste for årsmøte skal ha følgende faste punkter: 

  1. Valg av ordstyrer 
  2. Valg av referent 
  3. Valg av protokollunderskrivere 
  4. Regnskap fra forrige semester 
  5. Budsjett for kommende semester 
  6. Semesterberetning/leders beretning 
  7. Valg av styremedlemmer 
  8. Eventuelt 

5.7 Årsmøtet ledes av en utpekt ordstyrer, og det er ikke krav om at denne personen er medlem av foreningen. 

## §6 Om valg 

6.1 En valgperiode strekker seg fra 01.08-30.06. 

6.2 Alle medlemmer av foreningen kan stille til valg. 

6.3 Alle valg ved årsmøte foretas ved skriftlig votering, dersom det ikke fremsettes forslag om annet. Hvis ingen kandidat oppnår minst halvparten av stemmene, skal en ny avstemning avholdes mellom de to kandidatene som fikk flest stemmer i første runde.  

6.4 De som blir valgt inn av årsmøtet forplikter seg til å sitte i minimum en periode, men frafall aksepteres ved gyldig grunn.  
